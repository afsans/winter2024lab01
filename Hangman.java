import java.util.Scanner;

public class Hangman{
	public static void main (String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Enter a word to play HangMan!");
		String word = reader.nextLine();
		
		//To hide the word to be guessed
		System.out.print("\033[H\033[2J");
		runGame(word);
	}

	//Is the gussed character in the word?
	//If yes return the position of the character
	//If not return -1
	public static int isLetterInWord(String word, char c){
		int position = 0;
		for (int i = 0; i<word.length(); i++){
			if (word.charAt(i)==(c)){ 
				position = position + i;
				return position;
			}
		}
		return -1;
	}

	//Turns all characters uppercase to make the program case insensitive
	public static char toUpperCase (char c){
		c = Character.toUpperCase(c);
		return c;
	}
	
	//The code to check if the character is "true" and outputs the word guessed so far
	//(if the correct letter was guessed it changes to true(default is false in runGame))
	public static void printWork(String word, boolean[] array){
		String output = "";
		for(int i= 0; i<word.length();i++){
			if (array[i]==true){
				output+=word.charAt(i);
			}
			else{
				output += "_";
			}
		}
		System.out.println("Your result so far is:" + output);
	}
	
	
	public static void runGame(String word){
		Scanner reader = new Scanner(System.in);
		boolean[] array = new boolean[word.length()];
		
		boolean allRight = false;


		//Defaulting the boolean array element to false 
		for (int i = 0; i< array.length; i++){
			array[i]=false;
		}
		printWork(word, array);
		
		
		
		boolean gameEnd = false;
		int lives = 6;
		
			//game is not over
			while (gameEnd==false){
				/*calling the isLetterInWord function to check if the letter guessed
				is really in the word or not*/
				System.out.println("Enter a letter! ");
				char letter = reader.next().charAt(0);
				int result = isLetterInWord(word, letter);
			
				//-1 == wrong letter == you lost a life
				if (result==-1){
					lives--;
					System.out.println("You have " +  lives +  " left");
				}
				
				/*using the isLetterInWord method to make sure they got the right letter in the 
				right position then change 
				the boolean array in the same position to true
				the characters are all changed to uppercase with the toUpperCase function*/
				boolean letterFound = false;
				for(int i=0; i<word.length(); i++){
					if(isLetterInWord(word, letter)!=-1){
						array[i]= true;
						letterFound = true;
					}
				}
				
				/*this is to make sure it only shows this message once if there are 
				multiple occurences of a certain letter
				if the above for loop finds out that a letter that was guessed is true
				it will change it's value to true and it will only output the number of lives once
				it also prints if their guess is right obviously*/
				if (letterFound){				
					System.out.println("You have " +  lives +  " left");
				}
	
				/*if one of the letters are false then the guess is wrong, the code stops 
				if one or multiple guesses aren't wrong that means they guessed the right letters*/
				for(int i =0; i<array.length;i++){
					if(array[i] == false){
						allRight = false;
						break;
					}else{
						allRight = true;
					}
				}
				
				// if they ran out of lives the get insulted
				printWork(word, array);
				if (lives==0){
					gameEnd = true;
					System.out.println("The word was " + word + " gitgud.");
					
					
				// if they won they get praised 
				}else if (allRight == true){
					gameEnd = true;
					System.out.println("The word was " + word + ". Good Job!! XD");
				}
					
			}	
		
	}
}

